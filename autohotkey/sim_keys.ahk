﻿#NoEnv  ; For performance and compatibility with future AutoHotkey releases.
#Warn   ; Enable warnings to assist with detecting common errors.

#SingleInstance force

SendMode Input  ; For new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

;----------------------------------------------------------------------
; Virtual Belfry

#ifWinActive ahk_class Virtual Belfry

F10::
Media_Prev::
    ControlGet btn, Enabled,, Button35
    if (btn = 0)
    {
        ; Start
        Send {Space}
    }
    return

F11::
Media_Play_Pause::
    ControlGet btn, Enabled,, Button35
    if (btn = 1)
    {
        ControlGetText btn, Button35
        if (btn = "Stand")
        {
            ; Go
            Send {+}
        }
    }
    return

F12::
Media_Next::
    ControlGet btn, Enabled,, Button35
    if (btn = 1)
    {
        ; Stop now
        Send {ESC}
    }
    return

; Footswitch #1
!^1::
    ControlGet btn, Enabled,, Button35
    if (btn = 0)
    {
        ; Start
        Send {Space}
    }
    else
    {
        ControlGetText btn, Button35
        if (btn = "Rounds")
        {
            ; Bob
            Send b
        }
    }
    return

; Footswitch #2
!^2::
    ControlGet btn, Enabled,, Button35
    if (btn = 1)
    {
        ControlGetText btn, Button35
        if (btn = "Stand")
        {
            ; Go
            Send {+}
        }
        else if (btn = "Rounds")
        {
            ; Single
            Send s
        }
    }
    return

; Footswitch #3
!^3::
    ControlGet btn, Enabled,, Button35
    if (btn = 1)
    {
        ; Stop now
        Send {ESC}
    }
    return

; Single button foot switch
!^4::
    ControlGet btn_state, Enabled,, Button35
    if (btn_state = 0)
    {
        ; Start
        Send {Space}
    }
    else
    {
        ControlGetText btn_text, Button35
        if (btn_text = "Stand")
        {
            ; Go
            Send {+}
        }
        else
        {
            ; Stop now
            Send {ESC}
        }
    }
    return

;----------------------------------------------------------------------
; Abel

#ifWinActive ahk_exe Abel3.exe

F10::
Media_Prev::
    ControlGetText btn, Button2
    if (btn = "Start")
    {
        ; Start
        Send {F9}
    }
    return

F11::
Media_Play_Pause::
    ControlGetText btn, Button2
    if (btn = "Go")
    {
        ; Go
        Send {F11}
    }
    return

F12::
Media_Next::
    ; Stop now
    Send {ESC}{ESC}
    return

; Footswitch #1
!^1::
    ControlGetText btn, Button2
    if (btn = "Start")
    {
        ; Start
        Send {F9}
    }
    else if (btn = "Rounds")
    {
        ; Bob
        Send {F5}
    }
    return

; Footswitch #2
!^2::
    ControlGetText btn, Button2
    if (btn = "Go")
    {
        ; Go
        Send {F11}
    }
    else if (btn = "Rounds")
    {
        ; Single
        Send {F6}
    }
    return

; Footswitch #3
!^3::
    ; Stop now
    Send {ESC}{ESC}
    return

; Single button foot switch
!^4::
    ControlGetText btn, Button2
    if (btn = "Start")
    {
        ; Start
        Send {F9}
    }
    else if (btn = "Go")
    {
        ; Go
        Send {F11}
    }
    else
    {
        Send {ESC}{ESC}
    }
    return

