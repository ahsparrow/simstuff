import pyaudio
import serial
import sys
import time
import wave

CHUNK = 1024
CHANNELS = 1
RATE = 48000
FORMAT = pyaudio.paInt16

def make_callback(wave_file):
    def callback(data, frame_count, time_info, status):
        wave_file.writeframes(data)
        return (None, pyaudio.paContinue)

    return callback

def calibrate_bagley(wave_file, serial_dev):
    p = pyaudio.PyAudio()

    wf = wave.open(wave_file, "wb")
    wf.setnchannels(CHANNELS)
    wf.setsampwidth(p.get_sample_size(FORMAT))
    wf.setframerate(RATE)

    stream = p.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK,
                    stream_callback=make_callback(wf))

    start_time = time.monotonic()
    stream.start_stream()

    ser = serial.Serial(serial_dev, baudrate=2400, timeout=5)
    while True:
        bytes = ser.read(1)
        tdelta = time.monotonic() - start_time
        print(bytes.decode('ascii'), tdelta)
        if not bytes:
            break

    stream.stop_stream()
    stream.close()

    p.terminate()
    wf.close()

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Bagley multi-bell calibration")
    parser.add_argument("serial_device")
    parser.add_argument("wave_file")
    args = parser.parse_args()

    calibrate_bagley(args.wave_file, args.serial_device)
